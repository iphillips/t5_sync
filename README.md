### T5 sync engine ###

The T5 synchronization service is a .NET service application that runs on a Windows instance. 
Its purpose is to synchronize changes from the t5master database on MySQL (sql01) to the 
original SQL Server t5master database. 

The SQL Server database will eventuallly be eliminated altogether, and this synchronizer 
should have a limited operational lifespan. 

Details are in [Confluence](https://thing5dev.atlassian.net/wiki/spaces/HEL/pages/81867587/T5+Synchronization+Service).

### How do I get set up? ###

Just open the project with Visual Studio (Community Edition is fine)

You can run the debug version of the project in debug mode (it runs as a process rather than a service).

*Deployment instructions*

Just copy the contents of the relevant bin folder to the server. 
You can use INSTSVC to register the service with Windows if no service is already registered.


### Who do I talk to? ###

* Ivan Phillips, Jay Dave