﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using NLog;

namespace DataSyncService
{
    static class Program
    {
        
        private static Logger logger = LogManager.GetCurrentClassLogger();


        static void RunNonService()
        {
            CancellationTokenSource TokenSource = new CancellationTokenSource();


            Service1.cancelled = false;
            // Pass the token to the cancelable operation.

            ThreadPool.QueueUserWorkItem(new WaitCallback(Service1.Actions), TokenSource.Token);

            Thread.Sleep(5000);

            logger.Info("Stopping service...");

            //TokenSource.Cancel();

            int count = 0;

            while ((count < 3000) && !Service1.cancelled)
            {
                count++;
                Thread.Sleep(5000);
            }
            logger.Info("Service stopped.");

        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {

#if DEBUG

            RunNonService();
#else
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new Service1()
            };
            ServiceBase.Run(ServicesToRun);
#endif
        }
    }
}
