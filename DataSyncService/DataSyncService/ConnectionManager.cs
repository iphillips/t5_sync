﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataSyncService
{
    class ConnectionManager
    {
        public string SourceConnectionString { get; set; }
        public string DestConnectionString { get; set; }
        public MySqlConnection SourceConnection { get; set; }
        public SqlConnection DestConnection { get; set; }
        public MySqlConnection ReadConnection { get; set; }


    }
}
