﻿using MySql.Data.MySqlClient;
using MySql.Data.Types;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DataSyncService
{
    class SQLServerTableWorker
    {
        public string DatabaseName { get; set; }
        public string TableName { get; set; }
        public SqlConnection Connection { get; set; }

        public bool IsSQLServer { get; set; }
        public string PrimaryKeyName { get; set; }

        SqlCommand _readCommand;

        SqlDataReader _reader;

        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static Logger backupLogger = LogManager.GetLogger("BackupLogger");

        public SQLServerTableWorker(string tableName, string databaseName, string primaryKey, SqlConnection connection)
        {
            TableName = tableName;
            DatabaseName = databaseName;
            PrimaryKeyName = primaryKey;
            Connection = connection;
            IsSQLServer = true;
        }


        public bool Delete(int keyValue)
        {
            bool success = false;
            string sql = "DELETE FROM ";
            if (IsSQLServer)
            {
                sql += "[" + DatabaseName + "].dbo.[" + TableName + "] WHERE [" + PrimaryKeyName + "] = " + keyValue.ToString();
            }
            else
            {
                sql += "`" + DatabaseName + "`.`" + TableName + "` WHERE `" + PrimaryKeyName + "` = " + keyValue.ToString();
            }

            try
            {
                SqlCommand _command = new SqlCommand(sql, Connection);
                _command.ExecuteNonQuery();
                _command = null;
                success = true;
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Cannot delete: ({0}) [{1}]", sql, e.Message));
            }

            return success;
        }


        public void BackupExistingRecord(Dictionary<string,object> values)
        {
            StringBuilder sb = new StringBuilder();

            try
            {
                sb.AppendLine("-- INSERT OVERWRITE BACKUP");
                sb.AppendLine(string.Format("-- Table {0} Key {1} = {2}", this.TableName, this.PrimaryKeyName, values[PrimaryKeyName]));
            }
            catch (Exception e) {

            }

            string sql = "INSERT INTO ";

            string colNames = string.Join(",", values.Keys.Where(s => s != PrimaryKeyName).Select(k => "[" + k + "]").ToArray());
            //string paramNames = string.Join(",", values.Keys.Select(s => "@" + s.Replace(" ", "_")).ToArray());
            string paramNames = string.Join(",", values.Keys.Where(s => s != PrimaryKeyName).Select(s => (s == null) ? "NULL" : "'" + values[s].ToString() + "'").ToArray());

            if (IsSQLServer)
            {
                sql = sql + "[" + DatabaseName + "].dbo.[" + TableName + "] (" + colNames + ") VALUES (" + paramNames + ")"
                + ";";
            }
            else
            {
                sql += "`" + DatabaseName + "`.`" + TableName + "` (" + colNames + ") VALUES (" + paramNames + ")";
            }

            try
            {
                sb.AppendLine(sql);
            }
            catch (Exception e)
            {

            }

            backupLogger.Error(sb.ToString());

        }
        public bool Insert(int keyValue, Dictionary<string, object> newValues)
        {
            bool success = false;
            string sql = "INSERT INTO ";

            string colNames = string.Join(",", newValues.Keys.Select(k => "[" + k + "]").ToArray());
            string paramNames = string.Join(",", newValues.Keys.Select(s => "@" + s.Replace(" ","_")).ToArray());

            if (IsSQLServer)
            {
                sql = "SET IDENTITY_INSERT [" + DatabaseName + "].dbo.[" + TableName + "] ON; "
                + sql + "[" + DatabaseName + "].dbo.[" + TableName + "] (" + colNames + ") VALUES (" + paramNames + ")"
                + "; SET IDENTITY_INSERT [" + DatabaseName + "].dbo.[" + TableName + "] OFF; ";
            }
            else
            {
                sql += "`" + DatabaseName + "`.`" + TableName + "` (" + colNames + ") VALUES (" + paramNames + ")";
            }


            try
            {
                SqlCommand _command = new SqlCommand(sql, Connection);

                foreach (string nom in newValues.Keys)
                {
                    object o = newValues[nom];
                    if (o != null)
                    {
                        if (o.GetType().ToString().Contains("MySqlDateTime"))
                        {
                            MySqlDateTime dt = (MySqlDateTime)o;
                            o = new SqlDateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second, dt.Millisecond);                           
                        }

                        if (o.GetType().ToString().Contains("SByte"))
                        {
                            if (nom.Equals("is_billable") && TableName.Equals("support_event_tracking$"))
                            {
                                o = ((SByte)o == 1) ? true : false;
                            }

                            if (TableName.Equals("support_event_detail$") && (nom.Equals("parts_adv_replacement") || nom.Equals("parts_return")))
                            {
                                o = ((SByte)o == 1) ? true : false;
                            }
                        }

                        if (o.GetType().ToString().Contains("UInt64"))
                        {
                            if (
                                (nom.Equals("parts_return") && TableName.Equals("support_event_detail$"))
                                ||
                                ((nom.Equals("event_billable_pref") || nom.Equals("event_markup_pref")) && TableName.Equals("vendors$"))
                                )
                            {
                                o = ((UInt64)o == 1) ? true : false;
                            }
                            else
                            {
                                //logger.Info(string.Format("{0}.{1} is type {2} value {3}", TableName, nom, o.GetType().ToString(), o));
                            }
                        }

                    }
                    SqlParameter p = new SqlParameter("@" + nom.Replace(" ", "_"), o ?? DBNull.Value);
                    _command.Parameters.Add(p);
                }
                _command.ExecuteNonQuery();
                _command = null;
                success = true;
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Cannot insert: ({0}) [{1}]", sql, e.Message));
            }

            return success;
        }

        public bool Update(int keyValue, Dictionary<string, object> newValues)
        {
            bool success = false;
            string sql = "UPDATE ";

            string sets;
            
            string paramNames = string.Join(",", newValues.Keys.Select(s => "@" + s.Replace(" ", "_")).ToArray());

            if (IsSQLServer)
            {
                sets = string.Join(",", newValues.Keys.Where(s => s != PrimaryKeyName && !(s == "did_id" && TableName == "did$")).Select(s => string.Format("[{0}] = @{1}", s, s.Replace(" ", "_"))).ToArray());
                if (sets == "")
                {
                    return false;
                }

                sql = "SET IDENTITY_INSERT [" + DatabaseName + "].dbo.[" + TableName + "] ON; " 
                    + sql + "[" + DatabaseName + "].dbo.[" + TableName + "] SET " + sets + " WHERE [" + PrimaryKeyName + "] = @primary_key_value"
                    + "; SET IDENTITY_INSERT [" + DatabaseName + "].dbo.[" + TableName + "] OFF;";
            }
            else
            {
                sets = string.Join(",", newValues.Keys.Select(s => string.Format("`{0}` = @{1}", s, s.Replace(" ", "_"))).ToArray());
                sql += "`" + DatabaseName + "`.`" + TableName + "` (" + sets + ") VALUES (" + paramNames + ")";
            }


            try
            {
                SqlCommand _command = new SqlCommand(sql, Connection);

                foreach (string nom in newValues.Keys)
                {
                    object o = newValues[nom];
                    if (o != null)
                    {
                        if (o.GetType().ToString().Equals("System.SByte"))
                        {
                            o = (((SByte)o) != 0) ? true : false;
                        }

                        if (o.GetType().ToString().Contains("SByte"))
                        {
                            if (nom.Equals("is_billable") && TableName.Equals("support_event_tracking$"))
                            {
                                o = ((SByte)o == 1) ? true : false;
                            }

                            if (TableName.Equals("support_event_detail$") && (nom.Equals("parts_adv_replacement") || nom.Equals("parts_return")))
                            {
                                o = ((SByte)o == 1) ? true : false;
                            }
                        }

                        if (o.GetType().ToString().Contains("UInt64"))
                        {
                            if (

                                (nom.Equals("parts_return") && TableName.Equals("support_event_detail$"))
                                ||
                                ((nom.Equals("event_billable_pref") || nom.Equals("event_markup_pref")) && TableName.Equals("vendors$"))
                                )
                            {
                                o = ((UInt64)o == 1) ? true : false;
                            }
                            else
                            {
                                //logger.Info(string.Format("{0}.{1} is type {2} value {3}", TableName, nom, o.GetType().ToString(), o));
                            }
                        }
                    }
                    SqlParameter p = new SqlParameter("@" + nom.Replace(" ", "_"), o ?? DBNull.Value);
                    _command.Parameters.Add(p);
                }

                SqlParameter pk = new SqlParameter("@primary_key_value", keyValue);
                _command.Parameters.Add(pk);

                _command.ExecuteNonQuery();

                success = true;
                _command = null;
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Cannot update: ({0}) [{1}]", sql, e.Message));
            }

            return success;
        }


        public bool Get(int keyValue, out Dictionary<string, object> values)
        {
            bool success = false;
            values = new Dictionary<string, object>();

            string sql = "SELECT * FROM ";

            if (IsSQLServer)
            {
                sql = sql + "[" + DatabaseName + "].dbo.[" + TableName + "] WHERE [" + PrimaryKeyName + "] = @" + PrimaryKeyName;
            }
            else
            {
                sql += "`" + DatabaseName + "`.`" + TableName + "` WHERE `" + PrimaryKeyName + "` = @" + PrimaryKeyName;
            }            

            SqlParameter p;

            _readCommand = new SqlCommand(sql, Connection);
            p = new SqlParameter("@" + PrimaryKeyName, keyValue);
            _readCommand.Parameters.Add(p);
            //}
            //else
            //{
            //    p = _readCommand.Parameters[0];
            //    p.Value = keyValue;
            //}

            SqlDataReader reader = null;

            try { 
                reader = _readCommand.ExecuteReader();

                if (!reader.HasRows)
                {
                    return false;
                }

                reader.Read();
                
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    values.Add(reader.GetName(i), reader.GetValue(i));
                }

                success = true;

                
                
            }
            catch (Exception e)
            {
                Console.Write(string.Format("Cannot update: ({0}) [{1}]", sql, e.Message));
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
                reader = null;
                _readCommand = null;
            }
            return success;
        }

        public void CloseReader()
        {
            if (_reader != null)
            {
                try
                {
                    _readCommand.Cancel();
                }
                catch (Exception e)
                {

                }
                _reader.Close();
                _reader = null;
                _readCommand = null;
            }

        }

        public bool GetNext(int minIndex, bool newConnection, out Dictionary<string, object> values)
        {
            bool success = false;
            values = new Dictionary<string, object>();

            if (_readCommand == null)
            {

                if (!newConnection)
                {
                    return false;
                }

                string sql = "SELECT * FROM ";

                if (IsSQLServer)
                {
                    sql = sql + "[" + DatabaseName + "].dbo.[" + TableName + "] WHERE [" + PrimaryKeyName + "] >= @" + PrimaryKeyName + " ORDER BY [" + PrimaryKeyName + "]";
                }
                else
                {
                    sql += "`" + DatabaseName + "`.`" + TableName + "` WHERE `" + PrimaryKeyName + "` >= @" + PrimaryKeyName;
                }

                SqlParameter p;

                try
                {
                    _readCommand = new SqlCommand(sql, Connection);
                    p = new SqlParameter("@" + PrimaryKeyName, minIndex);
                    _readCommand.Parameters.Add(p);

                    _reader = _readCommand.ExecuteReader();

                }
                catch (Exception ee)
                {
                    logger.Error(ee.Message);
                    return false;
                }

            }
            //}
            //else
            //{
            //    p = _readCommand.Parameters[0];
            //    p.Value = keyValue;
            //}

            try
            {

                if (!_reader.HasRows)
                {
                    CloseReader();
                    _reader = null;
                    _readCommand = null;
                    return false;
                }

                if (_reader.Read())
                {
                    for (int i = 0; i < _reader.FieldCount; i++)
                    {
                        values.Add(_reader.GetName(i), _reader.GetValue(i));
                    }

                    success = true;
                }
                else
                {
                    success = false;
                }

            }
            catch (Exception e)
            {
                logger.Error(string.Format("Cannot read [{0}]", e.Message));
            }

            return success;
        }

    }



    class MySQLTableWorker
    {
        public string DatabaseName { get; set; }
        public string TableName { get; set; }
        public MySqlConnection Connection { get; set; }

        public string PrimaryKeyName { get; set; }

        MySqlCommand _readCommand;

        MySqlDataReader _reader;

        Int64 LastQueryIndex = 1;
        int RowsRead = 0;

        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static Logger backupLogger = LogManager.GetLogger("BackupLogger");

        public MySQLTableWorker(string tableName, string databaseName, string primaryKey, MySqlConnection connection)
        {
            TableName = tableName;
            DatabaseName = databaseName;
            PrimaryKeyName = primaryKey;
            Connection = connection;
        }


        public void BackupExistingRecord(Dictionary<string, object> values)
        {
            StringBuilder sb = new StringBuilder();

            try
            {
                sb.AppendLine("-- INSERT OVERWRITE BACKUP");
                sb.AppendLine(string.Format("-- Table {0} Key {1} = {2}", this.TableName, this.PrimaryKeyName, values[PrimaryKeyName]));
            }
            catch (Exception e)
            {

            }

            string sql = "INSERT INTO ";

            string colNames = string.Join(",", values.Keys.Where(s => s != PrimaryKeyName).Select(k => "[" + k + "]").ToArray());
            string paramNames = string.Join(",", values.Keys.Where(s => s != PrimaryKeyName).Select(s => "'" + values[s].ToString() + "'").ToArray());

            sql += "`" + DatabaseName + "`.`" + TableName + "` (" + colNames + ") VALUES (" + paramNames + ");";

            try
            {
                sb.AppendLine(sql);
            }
            catch (Exception e)
            {

            }

            backupLogger.Error(sb.ToString());

        }

        public bool Delete(int keyValue)
        {
            bool success = false;
            string sql = "DELETE FROM ";
            sql += "`" + DatabaseName + "`.`" + TableName + "` WHERE `" + PrimaryKeyName + "` = " + keyValue.ToString();

            try
            {
                MySqlCommand _command = new MySqlCommand(sql, Connection);
                _command.ExecuteNonQuery();
                success = true;
            }
            catch (Exception e)
            {
                Console.Write(string.Format("Cannot delete: ({0}) [{1}]", sql, e.Message));
            }

            return success;
        }

        public bool Insert(int keyValue, Dictionary<string, object> newValues)
        {
            bool success = false;
            string sql = "INSERT INTO ";

            string colNames = string.Join(",", newValues.Keys.ToArray());
            string paramNames = string.Join(",", newValues.Keys.Select(s => "@" + s.Replace(" ", "_")).ToArray());
            sql += "`" + DatabaseName + "`.`" + TableName + "` (" + colNames + ") VALUES (" + paramNames + ")";


            try
            {
                MySqlCommand _command = new MySqlCommand(sql, Connection);

                foreach (string nom in newValues.Keys)
                {
                    MySqlParameter p = new MySqlParameter("@" + nom.Replace(" ", "_"), newValues[nom]);
                    _command.Parameters.Add(p);
                }
                _command.ExecuteNonQuery();

                success = true;
            }
            catch (Exception e)
            {
                Console.Write(string.Format("Cannot insert: ({0}) [{1}]", sql, e.Message));
            }

            return success;
        }

        public bool Update(int keyValue, Dictionary<string, object> newValues)
        {
            bool success = false;
            string sql = "INSERT INTO ";

            string sets;

            string paramNames = string.Join(",", newValues.Keys.Select(s => "@" + s.Replace(" ", "_")).ToArray());

            sets = string.Join(",", newValues.Keys.Select(s => string.Format("`{0}` = @{1}", s, s.Replace(" ", "_"))).ToArray());
            sql += "`" + DatabaseName + "`.`" + TableName + "` (" + sets + ") VALUES (" + paramNames + ")";


            try
            {
                MySqlCommand _command = new MySqlCommand(sql, Connection);

                foreach (string nom in newValues.Keys)
                {
                    MySqlParameter p = new MySqlParameter("@" + nom.Replace(" ", "_"), newValues[nom]);
                    _command.Parameters.Add(p);
                }
                _command.ExecuteNonQuery();

                success = true;
            }
            catch (Exception e)
            {
                Console.Write(string.Format("Cannot update: ({0}) [{1}]", sql, e.Message));
            }

            return success;
        }


        public bool Get(int keyValue, out Dictionary<string, object> values)
        {
            bool success = false;
            values = new Dictionary<string, object>();

            string sql = "SELECT * FROM ";

            sql += "`" + DatabaseName + "`.`" + TableName + "` WHERE `" + PrimaryKeyName + "` = @" + PrimaryKeyName;

            MySqlParameter p;

            if (_readCommand == null)
            {
                _readCommand = new MySqlCommand(sql, Connection);
                p = new MySqlParameter("@" + PrimaryKeyName, keyValue);
                _readCommand.Parameters.Add(p);
            }
            else
            {
                p = _readCommand.Parameters[0];
                p.Value = keyValue;
            }

            MySqlDataReader reader = null;

            try
            {
                reader = _readCommand.ExecuteReader();

                if (!reader.HasRows)
                {
                    reader.Close();
                    return true;
                }

                reader.Read();

                for (int i = 0; i < reader.FieldCount; i++)
                {
                    object o = null;
                    try
                    {
                        o = reader.GetValue(i);
                    }
                    catch (Exception valueEx)
                    {
                        if (!valueEx.Message.Equals("Unable to convert MySQL date/time value to System.DateTime"))
                        {
                            throw valueEx;
                        }
                    }
                    
                    values.Add(reader.GetName(i), o);
                }

                success = true;
                
            }
            catch (Exception e)
            {
                Console.Write(string.Format("Cannot read: ({0}) key: {2} [{1}]", sql, e.Message, keyValue));
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }

            return success;
        }

        public void CloseReader()
        {
            if (_readCommand != null)
            {
                try
                {
                    _readCommand.Cancel();
                }
                catch (Exception e)
                {
                    logger.Error(e.Message);
                    logger.Info(e.StackTrace);
                }

                if (_reader != null)
                {
                    try
                    {
                        _reader.Close();
                    }
                    catch (Exception e)
                    {
                        if (!e.Message.Equals("No database selected"))
                        {
                            logger.Error(e.Message);
                            logger.Info(e.StackTrace);
                        }
                    }
                }

                _reader = null;
                _readCommand = null;
                LastQueryIndex = 1;
            }

        }

        public bool GetNext(int minIndex, bool newConnection, out Dictionary<string, object> values)
        {
            bool success = false;
            values = new Dictionary<string, object>();

            if (_readCommand == null)
            {
                if (!newConnection)
                {
                    values = null;
                    return false;
                }

                string sql = "SELECT * FROM ";

                sql += "`" + DatabaseName + "`.`" + TableName + "` WHERE `" + PrimaryKeyName + "` >= @" + PrimaryKeyName + " ORDER BY `" + PrimaryKeyName + "`";

                MySqlParameter p;            

                try
                {
                    _readCommand = new MySqlCommand(sql, Connection);
                    long index = minIndex > LastQueryIndex ? minIndex : LastQueryIndex;
                    
                    p = new MySqlParameter("@" + PrimaryKeyName, index);
                    _readCommand.Parameters.Add(p);

                    _readCommand.Prepare();


                    _reader = _readCommand.ExecuteReader();

                    if (_reader == null)
                    {
                        Thread.Sleep(4000);
                        _reader = _readCommand.ExecuteReader();

                    }
                    RowsRead = 0;

                    if (_reader == null)
                    {
                        CloseReader();
                        values = null;
                        return false;

                    }
                }
                catch (Exception ee)
                {
                    logger.Error(ee.Message);
                    CloseReader();
                    values = null;                    
                    return false;
                }

            }
            //}
            //else
            //{
            //    p = _readCommand.Parameters[0];
            //    p.Value = keyValue;
            //}

            try
            {

                if (!_reader.HasRows)
                {
                    CloseReader();
                    values = null;

                    if (RowsRead == int.MaxValue)
                    {
                        return GetNext(minIndex, true, out values);
                    }
                    return false;
                }

                if (_reader.Read())
                {
                    RowsRead++;
                    for (int i = 0; i < _reader.FieldCount; i++)
                    {
                        string colName = _reader.GetName(i);
                        object o = null;
                        try
                        {
                            o = _reader.GetValue(i);
                        }
                        catch (Exception valueEx)
                        {
                            if (!valueEx.Message.Equals("Unable to convert MySQL date/time value to System.DateTime"))
                            {
                                throw valueEx;
                            }
                        }
                        values.Add(colName, o);

                        if (colName.Equals(PrimaryKeyName))
                        {
                            LastQueryIndex = TableSynchronizer.ConvertToInt64(_reader.GetValue(i), int.MaxValue);
                        }
                    }

                    success = true;

                }
                else
                {
                    CloseReader();
                    values = null;
                    if (RowsRead == int.MaxValue)
                    {
                        return GetNext(minIndex, true, out values);
                    }
                    success = false;
                }
               
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Cannot read [{0}]", e.Message));
                CloseReader();
                values = null;
                return false;
            }

            return success;
        }

    }


}
