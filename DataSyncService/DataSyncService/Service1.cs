﻿using MySql.Data.MySqlClient;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Net.Mail;
using NLog.Targets;

namespace DataSyncService
{
    public partial class Service1 : ServiceBase
    {

        static CancellationTokenSource TokenSource;
        public static bool cancelled;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private static DateTime LastEmailSentDateTime = DateTime.Now.AddHours(-2);
        private static DateTime LastFileWriteDateTime = DateTime.Now.AddHours(-2);
        private static string LogFilename = "";


        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            TokenSource = new CancellationTokenSource();

            logger.Info("Starting service...");

            // Pass the token to the cancelable operation.
            cancelled = false;
            ThreadPool.QueueUserWorkItem(new WaitCallback(Actions), TokenSource.Token);

            
        }

        protected override void OnStop()
        {

            logger.Info("Stopping service...");

            TokenSource.Cancel();

            int count = 0;

            while ((count < 10) && !cancelled)
            {
                count++;
                Thread.Sleep(1000);
            }
            logger.Info("Service stopped.");

        }

        public static string GetAppSetting(string name, string defaultValue = "")
        {
            string s = defaultValue;
            if (ConfigurationManager.AppSettings.AllKeys.Contains(name))
            {
                s = ConfigurationManager.AppSettings[name];
            }

            return s;
        }


        public static string GetLastChars(string filename, int numChars)
        {
            var fileInfo = new FileInfo(filename);
            long len = (fileInfo.Length < numChars) ? fileInfo.Length : numChars;
            long start = (fileInfo.Length < numChars) ? 0 : fileInfo.Length - numChars;

            using (var stream = File.OpenRead(filename))
            {
                stream.Seek(start, SeekOrigin.Begin);
                using (var textReader = new StreamReader(stream))
                {
                    return textReader.ReadToEnd();
                }
            }
        }

        private static void EmailLogExcerpt()
        {
            string recipients = ConfigurationManager.AppSettings["AlertRecipients"];
            SmtpClient client = new SmtpClient("cloud5-com.mail.protection.outlook.com");

            MailMessage message = new MailMessage();
            string[] recips = recipients.Split(',');
            foreach (string r in recips)
            {
                message.To.Add(r);
            }
            message.From = new MailAddress("ivan.phillips@cloud5.com");
            message.Subject = "t5master database sync log messages";

            string s = GetLastChars(LogFilename, 3000);

            message.Body = s;
            // Credentials are necessary if the server requires the client 
            // to authenticate before it will send e-mail on the client's behalf.
            client.UseDefaultCredentials = true;

            try
            {
                client.Send(message);
            }
            catch (Exception e)
            {

            }
        }


        private static void LogAlert()
        {
            if (LogFilename.Equals(""))
            {
                return;
            }

            if ((DateTime.Now - LastEmailSentDateTime).TotalMinutes > 60)
            {
                DateTime fileTime = File.GetLastWriteTime(LogFilename);

                if ((fileTime- LastFileWriteDateTime).TotalMinutes > 1)
                {

                    EmailLogExcerpt();
                    LastEmailSentDateTime = DateTime.Now;
                    LastFileWriteDateTime = fileTime;
                }

                

            }
        }

        private static void InitLogFilename()
        {
            try
            {

                var fileTarget = (FileTarget)LogManager.Configuration.FindTargetByName("logfile");
                // Need to set timestamp here if filename uses date. 
                // For example - filename="${basedir}/logs/${shortdate}/trace.log"

                var logEventInfo = new LogEventInfo { TimeStamp = DateTime.Now };
                LogFilename = fileTarget.FileName.Render(logEventInfo);

                if (!LogFilename.Contains("\\"))
                {
                    LogFilename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, LogFilename);
                }

                if (!File.Exists(LogFilename))
                    throw new Exception("Log file " + LogFilename + " does not exist.");

            }
            catch (Exception e)
            {
                logger.Error(e, "Log Filename Init failed");
                logger.Info(e.Message);
                logger.Info(e.StackTrace);

            }
        }

        public static void Actions(object obj)
        {

            DateTime LastValidation = DateTime.MinValue;
            double validationPeriodHours = 6;
            if (LogFilename.Equals(""))
            {
                InitLogFilename();
            }



            validationPeriodHours = double.Parse(GetAppSetting("ValidationPeriodHours", "6"));

            int delayBetweenCyclesSec = Int32.Parse(GetAppSetting("DelayBetweenCyclesSeconds", "30000"));            

            CancellationToken token = (CancellationToken)obj;

            do
            {
                try
                {
                    ConnectionManager manager = new ConnectionManager();

                    manager.DestConnectionString = GetAppSetting("DestinationConnectionString", @"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=t5master_raw;MultipleActiveResultSets=true;User Id=banana;Password=abc123;PWD=abc123");
                    manager.SourceConnectionString = GetAppSetting("SourceConnectionString", @"Server=127.0.0.1;Uid=root;Pwd=Wrap2Book;");
                    manager.SourceConnection = new MySqlConnection(manager.SourceConnectionString);
                    manager.ReadConnection = new MySqlConnection(manager.SourceConnectionString);
                    manager.DestConnection = new SqlConnection(manager.DestConnectionString);

                    manager.SourceConnection.Open();
                    manager.ReadConnection.Open();
                    
                    manager.DestConnection.Open();

                    TableSynchronizer sync = new TableSynchronizer("t5master", manager);

                    try
                    {
                        if (GetAppSetting("SyncEnabled", "Yes").Equals("Yes"))
                        {
                            sync.ReadAndApplyChanges();
                        }


                        if ((DateTime.Now - LastValidation).TotalHours >= validationPeriodHours)
                        {
                            LastValidation = DateTime.Now;
                            if (GetAppSetting("ValidationEnabled", "No").Equals("Yes"))
                            {
                                sync.ValidateAllTables();
                            }
                        }

                    }
                    catch (Exception tle)
                    {
                        logger.Error(tle, "Read and/or validate failed.");
                        logger.Info(tle.Message);
                        logger.Info(tle.StackTrace);

                    }


                    manager.SourceConnection.Close();
                    manager.ReadConnection.Close();
                    manager.DestConnection.Close();

                }
                catch (Exception e)
                {
                    logger.Error(e, "Action loop failed.");
                    logger.Info(e.Message);
                    logger.Info(e.StackTrace);
                }
                LogAlert();
                Thread.Sleep(30000);


            } while (!token.IsCancellationRequested);

            cancelled = true;
        }
    }
}
