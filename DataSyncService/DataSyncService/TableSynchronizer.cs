﻿using MySql.Data.MySqlClient;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataSyncService
{
    class TableSynchronizer
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static Logger validationLogger = LogManager.GetLogger("ValidationLogger");

        public Dictionary<string,MySQLTableWorker> SourceTables { get; set; }
        public Dictionary<string,SQLServerTableWorker> DestTables { get; set; }
        public string PrimaryKeyName { get; set; }
        public string SourceTableName { get; set; }
        public string SourceDatabaseName { get; set; }
        public string DestTableName { get; set; }
        public string DestDatabaseName { get; set; }
        public ConnectionManager Connections { get; set; }

        public Dictionary<string, int> MinimumIndices { get; set; }

        private static bool _firstRun = true;

        private DateTime _lastConnectionRefresh = DateTime.Now;

        public TableSynchronizer(string sourceDatabaseName, ConnectionManager connections)
        {

            string filename = Service1.GetAppSetting("TriggerTemplateFile", @"C:\Users\Ivan\Documents\Cloud5\TriggerTemplate.txt");

            string template = "";
            StringBuilder script = new StringBuilder();

            if (File.Exists(filename))
            {
                template = File.ReadAllText(filename);
            }

            Connections = connections;
            SourceDatabaseName = sourceDatabaseName;

            _lastConnectionRefresh = DateTime.Now;

            SourceTables = new Dictionary<string, MySQLTableWorker>();
            DestTables = new Dictionary<string, SQLServerTableWorker>();
            MinimumIndices = new Dictionary<string, int>();


            string sql = "SELECT * FROM `" + SourceDatabaseName + "`.`tb_sync_list` WHERE primary_key is not null and primary_key != ''";

            MySqlCommand command = new MySqlCommand(sql, connections.SourceConnection);

            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                string srcT = reader.GetString(1);
                string srcD = reader.GetString(2);
                string dstT = reader.GetString(3);
                string dstD = reader.GetString(4);
                string primaryKey = reader.GetString(5);
                int minIndex = reader.GetInt32(6);

                //logger.Info(string.Format("Sync table {0}.{1} to {2}.{3} on {4}", srcD, srcT, dstD, dstT, primaryKey));

                SourceTables.Add(srcT, new MySQLTableWorker(srcT, srcD, primaryKey, connections.SourceConnection));
                DestTables.Add(srcT, new SQLServerTableWorker(dstT, dstD, primaryKey, connections.DestConnection));
                MinimumIndices.Add(srcT, minIndex);

                if (_firstRun)
                {
                    if (template.Length > 0)
                    {
                        script.Append(template.Replace("#TABLENAME#", srcT).Replace("#PRIMARYKEY#", primaryKey));
                    }
                }
            }

            reader.Close();

            command = null;

            if (_firstRun)
            {
                logger.Info("Writing triggers to file " + filename + ".sql");

                if (template.Length > 0)
                {
                    File.WriteAllText(filename + ".sql", script.ToString());
                }

            }

            _firstRun = false;
        }

        private bool RefreshConnections()
        {
            try
            {
                foreach (string key in SourceTables.Keys)
                {
                    SourceTables[key].CloseReader();
                }

                foreach (string key in DestTables.Keys)
                {
                    DestTables[key].CloseReader();
                }


                Connections.SourceConnection.Close();
                Connections.ReadConnection.Close();
                Connections.DestConnection.Close();

                foreach (string key in SourceTables.Keys)
                {
                    SourceTables[key].CloseReader();
                }

                foreach (string key in DestTables.Keys)
                {
                    DestTables[key].CloseReader();
                }

                Connections.SourceConnection.Open();
                Connections.ReadConnection.Open();
                Connections.DestConnection.Open();
                return true;

            }
            catch (Exception e)
            {
                logger.Error(string.Format("Error {0} while resetting connections. {1}", e.Message, e.StackTrace));
            }
            return false;
        }
        public void ValidateAllTables()
        {
            foreach (string skey in SourceTables.Keys)
            {
                ValidateTables(skey, MinimumIndices[skey]);
                if ((DateTime.Now - _lastConnectionRefresh).TotalMinutes >= 3.0)
                {
                    logger.Info("Refreshing connections...");
                    if (!RefreshConnections())
                    {
                        break;
                    }
                    _lastConnectionRefresh = DateTime.Now;
                }
            }
        }

        public bool ReadAndApplyChanges()
        {
            bool ok = true;

            string sqlChanges = "SELECT * FROM `" + SourceDatabaseName + "`.`tb_changes`";

            MySqlCommand changesCommand = new MySqlCommand(sqlChanges, Connections.ReadConnection);

            MySqlDataReader changesReader = changesCommand.ExecuteReader();

            if (!changesReader.HasRows)
            {
                return true;
            }


            Dictionary<string, object> values;
            Dictionary<string, object> destValues;
            List<int> changes = new List<int>();
            int changeIndex = 0;
            string lastTableName = "";

            while (changesReader.Read())
            {                

                
                string tableName = changesReader.GetString(0); //table name
                int keyIndex = changesReader.GetInt32(1); //keyIndex
                char changeType = changesReader.GetChar(2); //changeType
                DateTime dt = changesReader.GetDateTime(3); //changeDate
                int pkey = changesReader.GetInt32(4); //pkey

                //Console.WriteLine(string.Format("{0} {1} {2}", keyIndex, changeType, tableName));

                bool transactionOK = false;

                MySQLTableWorker SourceTable;
                if (SourceTables.ContainsKey(tableName))
                {
                    SourceTable = SourceTables[tableName];
                }
                else
                {
                    logger.Error("Missing source table worker: " + tableName);
                    continue;
                }

                SQLServerTableWorker DestTable;
                if (DestTables.ContainsKey(tableName))
                {
                    DestTable = DestTables[tableName];
                }
                else
                {
                    logger.Error("Missing destination table worker: " + tableName);
                    continue;
                }

                lastTableName = tableName;

                bool readSuccess = false;
                try
                {
                    switch (changeType)
                    {
                        case 'i': //Insert
                            readSuccess = SourceTable.Get(keyIndex, out values);

                            if (!readSuccess)
                            {
                                break;
                            }

                            if (readSuccess && values.Count == 0)
                            {
                                //source record not found - consider transaction ok
                                logger.Warn(string.Format("Record deleted before insert could be synced. Table {0} keyIndex {1}", tableName, keyIndex));
                                transactionOK = true;
                                break;
                            }

                            if (DestTable.Get(keyIndex, out destValues))
                            {
                                DestTable.BackupExistingRecord(destValues);

                                string difference = "";
                                if (CompareValues(values, destValues, out difference) == 0)
                                {
                                    //No differences, so do nothing
                                }
                                else
                                {
                                    //Try to delete the mismatch so that an insert will work
                                    DestTable.Delete(keyIndex);
                                    transactionOK = DestTable.Insert(keyIndex, values);
                                }
                            }
                            else
                            {
                                //No existing row, so perform insert
                                transactionOK = DestTable.Insert(keyIndex, values);
                            }
                            
                            break;
                        case 'u': //Update
                            readSuccess = SourceTable.Get(keyIndex, out values);

                            if (!readSuccess)
                            {
                                break;
                            }

                            if (readSuccess && values.Count == 0)
                            {
                                //source record not found - consider transaction ok
                                logger.Warn(string.Format("Record deleted before update could be synced. Table {0} keyIndex {1}", tableName, keyIndex));
                                transactionOK = true;
                                break;
                            }

                            if (DestTable.Get(keyIndex, out destValues))
                            {
                                transactionOK = DestTable.Update(keyIndex, values);
                            }
                            else
                            {
                                logger.Warn(string.Format("Record update processed as insert. Table {0} keyIndex {1}", tableName, keyIndex));
                                transactionOK = DestTable.Insert(keyIndex, values);
                            }

                            break;
                        case 'd': //Delete
                            transactionOK = DestTable.Delete(keyIndex);
                            break;
                    }

                }
                catch (Exception e)
                {
                    logger.Error(e, string.Format("Transaction failure. Operation {0} Table {1} Key {2}.", changeType, tableName, keyIndex));
                }

                if (transactionOK)
                {
                    changes.Add(pkey);
                    changeIndex++;
                    if (changeIndex == 2048)
                    {
                        break;
                    }
                }
                
                

            }

            changesReader.Close();

            if (changeIndex > 0)
            {
                string inBits = string.Join(",", changes.Select(i => i.ToString()).ToArray());
                MySqlCommand delCommand = new MySqlCommand("DELETE FROM `" + SourceDatabaseName + "`.`tb_changes` WHERE pkey IN (" + inBits + ")", Connections.ReadConnection);
                delCommand.ExecuteNonQuery();
            }

            return ok;

        }



    
        public static Int64 ConvertToInt64(object key1, int defaultValue = int.MinValue)
        {

            Int64 ikey1 = 0;

            switch (key1.GetType().ToString())
            {
                case "System.Int32":
                    ikey1 = (int)key1;
                    break;
                case "System.Int64":
                    ikey1 = (Int64)key1;
                    break;
                case "System.Decimal":
                    ikey1 = (long)Math.Truncate((double)((Decimal)key1));
                    break;
                default:
                    return int.MinValue;
            }

            return ikey1;
        }

        private int CompareKeys(object key1, object key2)
        {

            Int64 ikey1;
            Int64 ikey2;

            try
            {
                ikey1 = ConvertToInt64(key1);
            }
            catch (Exception e)
            {
                return int.MinValue;
            }

            try
            {
                ikey2 = ConvertToInt64(key2, int.MaxValue);
            }
            catch (Exception ee)
            {
                return int.MaxValue;
            }

            if (ikey1 == ikey2) return 0;

            if (ikey1 > ikey2) return 1;

            //if (ikey1 < ikey2) 
            return -1;
        }

        private int CompareValues(Dictionary<String,object> src, Dictionary<String, object> dest, out string firstDiff)
        {
            firstDiff = "";
            foreach (string key in src.Keys)
            {
                string a = (src[key] ?? "").ToString().Replace("\r\n","\n").Trim();
                string b = "";
                if (dest.ContainsKey(key))
                {
                    b = dest[key].ToString().Replace("\r\n", "\n").Trim();
                }
                else
                {
                    continue;
                }
                

                if (!a.Equals(b))
                {
                    if (a.Equals("1") && b.Equals("True"))
                    {
                        continue;
                    }
                    if (a.Equals("0") && b.Equals("False"))
                    {
                        continue;
                    }

                    firstDiff = string.Format("field {0} ({1} != {2})", key, a, b);
                    return 1;
                }
            }

            return 0;
        }

        public void ValidateTables(string tableName, int minIndex = 1)
        {

            if (tableName.StartsWith("dynamic_query"))
            {
                return;
            }

            MySQLTableWorker SourceTable;
            if (SourceTables.ContainsKey(tableName))
            {
                SourceTable = SourceTables[tableName];
            }
            else
            {
                validationLogger.Error("Validation: missing source table worker: " + tableName);
                return;
            }

            SQLServerTableWorker DestTable;
            if (DestTables.ContainsKey(tableName))
            {
                DestTable = DestTables[tableName];
            }
            else
            {
                validationLogger.Error("Validation: missing destination table worker: " + tableName);
                return;
            }

            Dictionary<string, object> destData = null;
            Dictionary<string, object> srcData = null;

            bool newSourceConnection = true;
            bool newDestConnection = true;


            while (DestTable.GetNext(minIndex, newDestConnection, out destData))
            {
                newDestConnection = false;

                if (!destData.ContainsKey(DestTable.PrimaryKeyName))
                {
                    validationLogger.Warn(string.Format("Missing primary key column {0} in dest table {1}.", DestTable.PrimaryKeyName, tableName));
                    break;
                }

                if (srcData != null && !srcData.ContainsKey(SourceTable.PrimaryKeyName))
                {
                    validationLogger.Warn(string.Format("Missing primary key column {0} in source table {1}.", SourceTable.PrimaryKeyName, tableName));
                    break;
                }

                object obj1;
                object obj2;


                while (srcData == null || CompareKeys(srcData[SourceTable.PrimaryKeyName], destData[DestTable.PrimaryKeyName]) < 0)
                {
                    if (srcData != null)
                    {
                        try
                        {
                            obj1 = srcData[SourceTable.PrimaryKeyName];
                        }
                        catch (Exception en)
                        {
                            validationLogger.Warn(string.Format("Missing primary key column {0} in source table {1}.", SourceTable.PrimaryKeyName, tableName));
                            break;
                        }

                    }

                    try
                    {
                        obj2 = destData[DestTable.PrimaryKeyName];
                    }
                    catch (Exception en)
                    {
                        validationLogger.Warn(string.Format("Missing primary key column {0} in dest table {1}.", DestTable.PrimaryKeyName, tableName));
                        break;
                    }

                    if (!SourceTable.GetNext(minIndex, newSourceConnection, out srcData))
                    {
                        newSourceConnection = false;
                        validationLogger.Warn(string.Format("COMPARE {0} destination has record key {1} missing from source.", tableName, destData[DestTable.PrimaryKeyName]));
                        break;
                    }
                    else
                    {
                        newSourceConnection = false;
                        int diff = CompareKeys(srcData[SourceTable.PrimaryKeyName], destData[DestTable.PrimaryKeyName]);
                        if (diff < 0)
                        {
                            validationLogger.Warn(string.Format("COMPARE {0} destination has record key {1} missing from source.", tableName, destData[DestTable.PrimaryKeyName]));
                        }
                        else
                        {
                            string firstDiff;
                            if (diff == 0 && CompareValues(srcData, destData, out firstDiff) != 0)
                            {
                                validationLogger.Warn(string.Format("COMPARE DATA {0} destination has differing record values for key {1}, field {2}", tableName, destData[DestTable.PrimaryKeyName], firstDiff));
                            }
                        }
                    }

                }

            }

            DestTable.CloseReader();
            SourceTable.CloseReader();

        }


    }
}
