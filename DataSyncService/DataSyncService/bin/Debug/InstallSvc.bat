SET PATH=%path%;%windir%\Microsoft.NET\Framework64\v4.0.30319

REM Uninstall the Service if it exists already

InstallUtil /u DataSyncService.exe

REM Install the Service

InstallUtil /i DataSyncService.exe

REM Start the Service once we're done
net start T5DataSync
PAUSE